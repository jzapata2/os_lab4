#include "SharedObject.h"
#include "Semaphore.h"
#include "socket.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

bool gameState = true;

int main(void)
{
    std::string entry = "";
    std::string output = "";

    Socket theSocket("127.0.0.1", 2000); 
    int dummyCounter = 0;  

    try
    {
        theSocket.Open();
        while (gameState)
        {
            FlexWait waiter(2,&cinWatcher,&theSocket);
            Blockable * result = waiter.Wait();
            //CASE 1 OF THE BLOCKABLE
            //Case where the socket responds first (most likley with a READ initiated by the Server)
            if (result == &theSocket && gameState){
                
                ByteArray dummy;
                if (theSocket.Read(dummy) <= 0){
                    std::cout << "\nThe socket has been closed suddenly. Sorry for the inconvenience." << std::endl;
                    gameState = false;
                    break;
                }
                else{
                    output = dummy.ToString();
                    std::cout<<output<< std::endl;

                    if(output == "done")
                    {
                        std::cout<<output<< std::endl;
                        gameState = false;
                        break;
                    }
                }
            }
            //CASE 2 OF THE BLOCKABLE
            //Case where the console cin is first (getline)
            else if (gameState)
            {
                getline(cin, entry);
                ByteArray ba(entry);
                //Just to make the screen more readable for the player
                system("clear");

                int written = theSocket.Write(ba);

                if ( written != ba.v.size())
                {
                    std::cout << "Wrote: " << written << std::endl;
                    std::cout << "The socket appears to have been closed suddenly...!=" << std::endl;
                    gameState = false;
                    break;
                }

                if (theSocket.Read(ba) <=0)
                {
                    std::cout << "The socket appears to have been closed suddenly...<=0" << std::endl;
                    gameState = false;
                    break;
                }
                else{
                    
                    output = ba.ToString();
                    std::cout<<output << std::endl;

                    if (output == "done")
                    {
                        std::cout<<output << std::endl;
                        gameState = false;
                        break;
                    }
                }
            }
        }
        //Doesnt want to occurr if the game NATURALLY ends. 
        //It does occur if someone loggs off or the server shuts down.
        std::cout << "Sleep now" << std::endl;
        theSocket.Close();
        sleep(1);
    }
    catch(std::string s)
    {
        std::cout <<s << std::endl;
    }
    catch(...)
    {
        std::cout << "Caught unexpected exception" << std::endl;
    }

    cout<<"\nPlease join us next week on Wheel of Fortune!\n";
}
