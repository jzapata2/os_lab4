#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
#include "SharedObject.h"
#include "Semaphore.h"
#include "thread.h"
#include "socketserver.h"
#include <stdlib.h>
#include <ctime>
#include <cstdlib>
#include <time.h>
#include <list>
#include <pthread.h>
#include <vector>

using namespace std;

int const COMMENTATOR = -1;
int const EVERYONE = -1;

string welcomeMsg;

class CommThread : public Thread
{
private:
    Socket player1;
    Socket player2;
    int gameNum;
    string puzzle;
    string solved;
    bool gameState;
    int turnPlayer;

public:

    //WANT FOR VS COMPUTER GAME =============================================BONUS MARKS
    CommThread(Socket const & p1, int const &gameNo)
    : Thread(true),player1(p1), player2(0), gameNum(gameNo){
        gameState = true;
        turnPlayer = 1;
    }

    CommThread(Socket const & p1, Socket const & p2, int const &gameNo)
    : Thread(true),player1(p1), player2(p2), gameNum(gameNo) {
        gameState = true;
        turnPlayer = 1;
    }

//========================================================================================//
//======================================THREAD MAIN=======================================//
//========================================================================================//

    long ThreadMain(void)
    {
        welcomeMsg = "Welcome to Wheel of Fortune. Game #"+IntToString(gameNum)+" is about to start.\nPlease take your seats!";
        RandomizePuzzle();
        char pastGuesses[26] = {0};

        //the paramaters basically are saying "to: -1(as in both players), from: -1(as in the commentator of the game, and the message to send)"
        SendPlayer(EVERYONE,COMMENTATOR,welcomeMsg);
        SendPlayer(1,COMMENTATOR,"You are player 1.");
        SendPlayer(2,COMMENTATOR,"You are player 2.");

        while(gameState){

            SendPlayer(EVERYONE,COMMENTATOR,"TURN PLAYER ["+IntToString(turnPlayer)+"]\nEnter a single character for a guess at the puzzle.\nOr enter an entire string to attempt to solve it.\nTodays puzzle is\n"+solved);

            int readSize = 0;
            ByteArray bytes;
            std::string theString = "";

            //dont really want to use blockable stuff here because we only want one player
            //acting at a time
            if (turnPlayer == 1)
            {
                readSize = player1.Read(bytes);
            }
            else if(turnPlayer == 2){

                readSize = player2.Read(bytes);
            }
            else
                gameState = false;


            //lets validate what we read
            if (readSize == -1){
                std::cout << "Error in socket detected" << std::endl;                
                gameState = false;
            }
            //what if a player simply enters ""...as in just the enter key?
            else if (readSize == 0){

                SendPlayer(EVERYONE, COMMENTATOR, "Socket closed at remote end");
                Sleep(1);               //this fixed a dirty read problem
                std::cout << "Socket closed at remote end" << std::endl;

                //I think i need to send this because ONE of the players logged out.
                //now i need to log the other out
                SendBoth("done");
                gameState = false;
                break;
            }
            else{
                //a response of a player
                theString = bytes.ToString();
                //one of the two players sent "done"
                if(theString == "done")
                    gameState = false;        //for the sake of function TurnMove
                //have sendPlayer stuff inside here 
                //(basically dont want to output because the game should be ending?)
                if (gameState)                
                    TurnMove(readSize, theString, pastGuesses);
                if (!gameState || (puzzle == solved))
                    Sleep(1);               //this fixed a dirty read problem
                    SendBoth("done");                
            }
        }
        std::cout << "======Thread is gracefully ending======" << std::endl;       
    }
    ~CommThread(void){

        SendBoth("done");
        terminationEvent.Wait();
        player1.Close();
        player2.Close();
    }


//========================================================================================//
//=======================THE LOGIC OF EACH PLAYERS'S MOVE=================================//
//========================================================================================//


    void TurnMove(int readSize, string yourGuess, char * pastGuesses)
    {
        bool moveAction = false;

        MinimizeInput(yourGuess);

        if(readSize == 1){
            SendPlayer(EVERYONE,turnPlayer,"I want to guess a single character with the guess: '"+yourGuess+"'.");
            PutInGuessedChars(yourGuess[0], pastGuesses, moveAction);
        }
        else if (readSize >1){

            SendPlayer(EVERYONE,turnPlayer,"I want to guess the entire puzzle with the guess:\n'"+yourGuess+"'.");

            if(yourGuess == puzzle){

                solved = puzzle;
                moveAction = true;
            }
        }

        //The move resulted in something relevent to the game
        if(moveAction)
            SendPlayer(EVERYONE, COMMENTATOR, "Nice! Player "+IntToString(turnPlayer)+"'s move worked!\nLets see what we solved.");
        else            
            SendPlayer(EVERYONE, COMMENTATOR, "Sorry! Player "+IntToString(turnPlayer)+"'s move failed!");
        
        if(solved == puzzle){

            SendPlayer(EVERYONE, COMMENTATOR, "Congrats! Player"+IntToString(turnPlayer)+" won the game!\nGAME OVER!");
            gameState = false;
        }

        if(turnPlayer == 1 && player2 != 0){turnPlayer=2;}
        else if (turnPlayer ==2){turnPlayer=1;}

        //This is for the vsAI game. 
        if((player2 == 0) && gameState){
            //The AI guesses a character (from a list of chars that have not yet been used)
            char AIguess = InsertRandChar(pastGuesses, moveAction);
            //turn the char into a string response
            string AIResponse;
            stringstream ss;
            ss << AIguess;
            ss>>AIResponse;

            SendPlayer(1,COMMENTATOR,"It is now the AI's turn. The AI guesses: "+AIResponse);

            if(solved == puzzle){

                SendPlayer(EVERYONE, COMMENTATOR, "Sorry! The AI won the game!\nGAME OVER!");
                gameState = false;
            }
        }
    }

    //update the previous character guesses
    bool PutInGuessedChars(char inputChar, char * thusFar, bool & moveAction){

        bool alreadyInArray = true;
        bool successfulAdd = false;

        //Update the puzzle
        for (int i = 0; i < puzzle.length(); ++i)
        {
            if (puzzle[i] == inputChar && solved[i] == '_')
            {
                solved[i] = inputChar;
                moveAction = true;
            }
        }
        //responsible for checking the already guessed characters
        for (int i = 0; i < 26; ++i){

            if (thusFar[i] == inputChar )
            {   //the char exists inside the array
                alreadyInArray = true;
                moveAction = false;
                break;
            }
            else
                alreadyInArray = false;
        }
        //responsible for updating the already guessed characters
        if (!alreadyInArray){
            thusFar[((int)inputChar%(int)'a')] = inputChar;
            successfulAdd = true;
        }
        return successfulAdd;
    }

    //because this is wheel of fortune
    void RandomizePuzzle(){

        //randomly generate a string from the text file to be built later
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        //=====================================================================
        puzzle = "AAA BBB CCC!!";
        MinimizeInput(puzzle);
        solved = puzzle;

        for (int i = 0; i < solved.length(); ++i)
        {
            //Cencor all the letters
            if((solved[i] >= 65 && solved[i] <= 90) || (solved[i] >= 97 && solved[i] <= 122))
                solved[i] = '_';                
        }
    }

    //Normalize the input (dont want conflicts due to upper case and lower case letters)
    string MinimizeInput(string & toShrink){

        char mini;

        for (int i = 0; i < toShrink.length(); ++i)
        {
            if((toShrink[i] >= 65 && toShrink[i] <= 90) || (toShrink[i] >= 97 && toShrink[i] <= 122)){
                        
                mini = toShrink[i];
                toShrink[i] = tolower(mini);
            }
        }

        return toShrink;
    }
    //Used to generate the char guess on behalf of the AI
    char InsertRandChar(char * thusFar, bool & moveAction){

        bool alreadyInArray = true;
        char randChar;

        srand((unsigned int)time(NULL));
        randChar = (char)((int)'a' + rand() % ((int)'z'-(int)'a'+1));

        while(PutInGuessedChars(randChar, thusFar, moveAction) == false){
            //maybe tell the turn player they need to guess again?
            randChar = (char)((int)'a' + rand() % ((int)'z'-(int)'a'+1));
        }

        // sendPlayer(1,COMMENTATOR,"The AI guessed: "+randChar);

        return randChar;
    }

//========================================================================================//
//======Stuff to simplify writing to turn players and telling them who wrote it===========//
//========================================================================================//

    //Send player 1 & player 2 a message, encapsulates the ByteArray stuff. No formatting occurs here
    void SendBoth(string const & s){

        ByteArray sendMSG(s);
        player1.Write(sendMSG);
        if(player2 != 0)
            player2.Write(sendMSG);
    }

    //send a specific player a certain message 
    //Some formatting goes on in here. Like what game it is coming from and say who sent it 
    //(either p1, p2, or commentator, maybe even AI...future goal)
    //using -1 for commentator and want to use -2 as AI
    void SendPlayer(int const & to, int const &from, string s){

        //USING THE '***' for the sake of differentiating the end of a formated .Write() [from SendPlayer(...)]
        //vs a nonformatted .Write() [from SendBoth(...)]

        string whatGame = "===============[From game: "+IntToString(gameNum);

        //from the commentator
        if(from == COMMENTATOR){

            s = whatGame+", Commentator]===============:\n"+s+"***\n";
        }
        else if(from >= 0){

            s = whatGame+", Player "+IntToString(from)+"]===============:\n"+s+"***\n";            
            cout<<s;
        }
        //from vsComputer
        else
        {
            s = whatGame+", AI]===============:\n"+s+"***\n";            
        }

        ByteArray sendMSG(s);                

        if (to == 1)        
            player1.Write(sendMSG);        
        else if(to == 2)
            player2.Write(sendMSG);        
        else if(to == COMMENTATOR)
            SendBoth(s);
    }


    //because i get some issue with concadinating ints into strings :P
    string IntToString(int number){

        string num = static_cast<ostringstream*>( &(ostringstream() << number) )->str();

        return num;
    }
};

int main(void)
{
    std::cout << "I am a socket server.  Type 'quit' to exit\nWould you like to start the 2 player game, or the 1 player game (vs AI)?\n[1: AI, 2: Two Player]" << std::endl;
    SocketServer theServer(2000);
    std::vector<CommThread *> threads;
    string serverInput = "";
    string amountOfPlayers = "";

    getline(cin, amountOfPlayers);

    //make sure sure the server ONLY accepts 1 player or 2 player games
    while(!(amountOfPlayers == "1" || amountOfPlayers == "2" )){

        std::cout << "Sorry, that is not an apporopriate response. [1: AI, 2: Two Player]" << std::endl;
        getline(cin, amountOfPlayers);
    }

    try
    {   
        while(true)
        {
            FlexWait waiter(2,&theServer,&cinWatcher);
            Blockable * result = waiter.Wait();
            
            if (result == &cinWatcher)
            {
                getline(cin, serverInput);

                if (serverInput == "quit")
                {
                    break;
                }

                else continue;                
            }
            //Prof Mc didnt use an 'else' or 'else if(&theServer)' in the solution to lab3. dont think we need it
                // Accept should not now block.
            Socket newSocket1 = theServer.Accept();

            if (amountOfPlayers == "2")
            {
                newSocket1.Write(ByteArray("Please wait for player 2.\n"));

                Socket newSocket2 = theServer.Accept();

                std::cout << "Received a socket connection!" << std::endl;
                threads.push_back(new CommThread(newSocket1, newSocket2, threads.size()+1));
            }
            else{

                newSocket1.Write(ByteArray("You will be playing agains an AI opponent.\n"));
                threads.push_back(new CommThread(newSocket1, threads.size()+1));
            }
        }
    }
    catch(TerminationException e)
    {
        std::cout << "EXCEPTION(e)! The socket server is no longer listening. Exiting now." << std::endl;
    }
    catch(std::string s)
    {
        std::cout << "THROWS(s)" << s << std::endl;
    }
    catch(...)
    {
        std::cout << "caught  unknown exception" << std::endl;
    }
    for (int i=0;i<threads.size();i++)
        delete threads[i];

    std::cout << "Sleep now" << std::endl;
    sleep(1);
    std::cout << "End of main" << std::endl;

}
